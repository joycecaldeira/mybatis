/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Update;
import produto.Produto;

/**
 *
 * @author Joyce
 */
public interface ProdutoMapper {

    @Insert("INSERT INTO "
			+ "produto(nome, descricao, qtde, valor)"
			+ "	VALUES(#{nome}, #{descricao}, "
			+ "#{qtde}, #{valor})")
	void insertProduto(Produto produto);
        
    @Delete("delete from produto where id = #{id}")
        void deleteProduto(int id);
        
    @Update("Update produto set nome=#{nome}, descricao=#{descricao}, qtde=#{qtde},valor=#{valor} where id=#{id}")
        void updateProduto(Produto produto);
}
