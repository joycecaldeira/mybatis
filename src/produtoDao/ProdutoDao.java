/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package produtoDao;

import conexao.Conexao;
import mapper.ProdutoMapper;
import org.apache.ibatis.session.SqlSession;
import produto.Produto;

/**
 *
 * @author Joyce
 */
public class ProdutoDao {
    public void save(Produto produto){
		SqlSession session = Conexao.getSqlSessionFactory().openSession();	
		ProdutoMapper mapper = session.getMapper(ProdutoMapper.class);
		mapper.insertProduto(produto);
		session.commit();
		session.close();
	}
    public void delete(int id){
        SqlSession session = Conexao.getSqlSessionFactory().openSession();	
        ProdutoMapper mapper = session.getMapper(ProdutoMapper.class);
        mapper.deleteProduto(id);
        session.commit();
	session.close();   
    }
    public void update(Produto produto){
         SqlSession session = Conexao.getSqlSessionFactory().openSession();	
        ProdutoMapper mapper = session.getMapper(ProdutoMapper.class);
        mapper.updateProduto(produto);
        session.commit();
	session.close();   
        
    }
}
